// tslint:disable-next-line: class-name
export interface USER_LOGIN {
  ID: number;
  EMAIL: string;
  ACCESS_ID: number;
  IS_DELETE: boolean;
}
