import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { ToastController } from '@ionic/angular';
import { USER_LOGIN } from 'src/interface/USER_LOGIN';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;
  is_login = true;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private service: LoginService,
    private toastController: ToastController
  ) {
    this.formGroup = this.fb.group({
      EMAIL: ['', Validators.required],
      PASSWORD: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (localStorage.getItem('user_token') != undefined) {
      this.router.navigateByUrl('/master/tabs/1/home');
    } else {
      this.is_login = false;
    }
  }

  onClickLogin() {
    console.log('formGroup', this.formGroup.value);
    if (this.formGroup.valid) {
      this.service.postLogin(this.formGroup.value).subscribe(
        (data: USER_LOGIN) => {
          if (data) {
            localStorage.setItem('user_token', JSON.stringify(data));
            this.toastMsg('Login success !');
            if (data.ACCESS_ID === 0) {
              this.router.navigateByUrl('/master/tabs/1/home');
            } else {
              this.router.navigateByUrl('/master/tabs/4/home');
            }
          } else {
            this.toastMsg('Not found email or password !');
          }
        },
        error => {
          this.toastMsg('Cannot connect to server !');
        }
      );
    } else {
      this.toastMsg('Please insert email and password !');
    }
  }

  async toastMsg(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      showCloseButton: true,
      position: 'bottom',
      closeButtonText: 'Ok',
      duration: 3000
    });
    toast.present();
  }
}
