import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private prefixUrl = `${environment.apiUrl}/USER_INFO`;

  constructor(private http: HttpClient) {}

  postLogin(form): Observable<any> {
    return this.http.post(`${this.prefixUrl}/getLogin`, form);
  }
}
