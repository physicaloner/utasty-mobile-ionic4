import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private service: RegisterService,
    private toastController: ToastController
  ) {
    this.formGroup = this.fb.group({
      EMAIL: ['', Validators.required],
      PASSWORD: ['', Validators.required],
      RE_PASSWORD: ['', Validators.required]
    });
  }

  ngOnInit() {}

  onClickRegister() {
    console.log('formGroup', this.formGroup.value);
    if (this.formGroup.valid) {
      this.service.postRegister(this.formGroup.value).subscribe(
        data => {
          if (data) {
            this.toastMsg('Register success !');
            this.router.navigateByUrl('/login');
          } else {
            this.toastMsg('Register failed !');
          }
        },
        error => {
          this.toastMsg('Register failed !');
        }
      );
    } else {
      this.toastMsg('Please insert email and password !');
    }
  }

  async toastMsg(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      showCloseButton: true,
      position: 'bottom',
      closeButtonText: 'Ok',
      duration: 3000
    });
    toast.present();
  }
}
