/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RegisterTraderService } from './register-trader.service';

describe('Service: RegisterTrader', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegisterTraderService]
    });
  });

  it('should ...', inject([RegisterTraderService], (service: RegisterTraderService) => {
    expect(service).toBeTruthy();
  }));
});
