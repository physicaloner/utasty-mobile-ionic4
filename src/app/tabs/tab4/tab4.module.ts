import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProfileEditorComponent } from './profile-editor/profile-editor.component';
import { HistoryOrderingComponent } from './history-ordering/history-ordering.component';
import { MainComponent } from './main/main.component';
import { Tab4Routes } from './tab4.routing';
import { ChatComponent } from './admin/chat/chat.component';
import { EditMasterComponent } from './superadmin/edit-master/edit-master.component';
import { EditNewsComponent } from './superadmin/edit-news/edit-news.component';
import { ApproveShopComponent } from './superadmin/approve-shop/approve-shop.component';
import { EditShopComponent } from './admin/edit-shop/edit-shop.component';
import { ViewApproveShopComponent } from './superadmin/approve-shop/view-approve-shop/view-approve-shop.component';

@NgModule({
  imports: [
    IonicModule,
    SharedModule,
    // RouterModule.forChild([{ path: '', component: Tab4Page }])
    Tab4Routes
  ],
  declarations: [
    MainComponent,
    ProfileEditorComponent,
    HistoryOrderingComponent,
    EditShopComponent,
    ChatComponent,
    EditMasterComponent,
    EditNewsComponent,
    ApproveShopComponent,
    ViewApproveShopComponent
  ]
})
export class Tab4PageModule {}
