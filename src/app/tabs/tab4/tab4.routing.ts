import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { ProfileEditorComponent } from './profile-editor/profile-editor.component';
import { HistoryOrderingComponent } from './history-ordering/history-ordering.component';
import { ChatComponent } from './admin/chat/chat.component';
import { EditMasterComponent } from './superadmin/edit-master/edit-master.component';
import { EditNewsComponent } from './superadmin/edit-news/edit-news.component';
import { ApproveShopComponent } from './superadmin/approve-shop/approve-shop.component';
import { EditShopComponent } from './admin/edit-shop/edit-shop.component';
import { ViewApproveShopComponent } from './superadmin/approve-shop/view-approve-shop/view-approve-shop.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: MainComponent },
      { path: 'profile-editor', component: ProfileEditorComponent },
      { path: 'history-ordering', component: HistoryOrderingComponent },
      { path: 'edit-shop', component: EditShopComponent },
      { path: 'chat', component: ChatComponent },
      { path: 'edit-master', component: EditMasterComponent },
      { path: 'edit-news', component: EditNewsComponent },
      {
        path: 'approve-shop',
        children: [
          { path: '', component: ApproveShopComponent },
          { path: 'view', component: ViewApproveShopComponent }
        ]
      }
    ]
  }
];

export const Tab4Routes = RouterModule.forChild(routes);
