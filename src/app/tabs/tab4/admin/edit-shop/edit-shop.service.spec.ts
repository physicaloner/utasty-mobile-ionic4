/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EditShopService } from './edit-shop.service';

describe('Service: EditShop', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditShopService]
    });
  });

  it('should ...', inject([EditShopService], (service: EditShopService) => {
    expect(service).toBeTruthy();
  }));
});
