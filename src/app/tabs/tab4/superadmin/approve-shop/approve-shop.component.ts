import { Component, OnInit } from '@angular/core';
import { USER_LOGIN } from 'src/interface/USER_LOGIN';
import { ApproveShopService } from './approve-shop.service';
import { SHOP_LIST } from 'src/interface/SHOP_LIST';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-approve-shop',
  templateUrl: './approve-shop.component.html',
  styleUrls: ['./approve-shop.component.scss']
})
export class ApproveShopComponent implements OnInit {
  TEMP_USER: USER_LOGIN;
  TEMP_SHOP: SHOP_LIST[];

  constructor(
    private service: ApproveShopService,
    private route: Router,
    public navCtrl: NavController
  ) {}

  ngOnInit() {
    this.TEMP_USER = JSON.parse(localStorage.getItem('user_token'));
    console.log('TEMP_USER', this.TEMP_USER);

    this.service.getShop(this.TEMP_USER.ID).subscribe(
      (data: SHOP_LIST[]) => {
        this.TEMP_SHOP = data;
      },
      error => {}
    );
  }

  onClick(id) {
    this.route.navigateByUrl('/master/tabs/4/approve-shop/view', {
      state: { id: id }
    });
  }
}
