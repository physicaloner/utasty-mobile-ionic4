/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ApproveShopService } from './approve-shop.service';

describe('Service: ApproveShop', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApproveShopService]
    });
  });

  it('should ...', inject([ApproveShopService], (service: ApproveShopService) => {
    expect(service).toBeTruthy();
  }));
});
