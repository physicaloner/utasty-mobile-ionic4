import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ViewApproveShopService } from './view-approve-shop.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-view-approve-shop',
  templateUrl: './view-approve-shop.component.html',
  styleUrls: ['./view-approve-shop.component.scss']
})
export class ViewApproveShopComponent implements OnInit {
  USER_ID: number;

  constructor(
    private router: Router,
    private service: ViewApproveShopService,
    public navCtrl: NavController
  ) {
    this.USER_ID = this.router.getCurrentNavigation().extras.state.id;
    console.log('USER_ID (queryParams)', this.USER_ID);
  }

  ngOnInit() {
    this.service.getShop(this.USER_ID).subscribe(data => {}, error => {});
  }

  onApprove() {
    this.navCtrl.pop();
  }

  onCancel() {
    this.navCtrl.pop();
  }
}
