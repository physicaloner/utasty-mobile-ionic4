/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ViewApproveShopService } from './view-approve-shop.service';

describe('Service: ViewApproveShop', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewApproveShopService]
    });
  });

  it('should ...', inject([ViewApproveShopService], (service: ViewApproveShopService) => {
    expect(service).toBeTruthy();
  }));
});
