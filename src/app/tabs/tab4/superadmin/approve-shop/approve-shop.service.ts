import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApproveShopService {
  private prefixUrl = `${environment.apiUrl}/USER_INFO`;

  constructor(private http: HttpClient) {}

  getShop(id): Observable<any> {
    return this.http.get(`${this.prefixUrl}/shopApproveList`);
  }
}
