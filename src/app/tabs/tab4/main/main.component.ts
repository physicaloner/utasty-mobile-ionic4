import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { USER_LOGIN } from 'src/interface/USER_LOGIN';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  ACCESS_ID = 0;
  TEMP_USER: USER_LOGIN;

  constructor(private router: Router, public navCtrl: NavController) {}

  ngOnInit() {
    this.TEMP_USER = JSON.parse(localStorage.getItem('user_token'));
    this.ACCESS_ID = this.TEMP_USER.ACCESS_ID;
    console.log('ACCESS_ID', this.ACCESS_ID);
  }

  onLogout() {
    localStorage.removeItem('user_token');
    this.router.navigate(['/login']);
  }

  onProfile() {
    this.navCtrl.navigateForward('/master/tabs/4/profile-editor');
  }

  onHistory() {
    this.navCtrl.navigateForward('/master/tabs/4/history-ordering');
  }

  onEditShop() {
    this.navCtrl.navigateForward('/master/tabs/4/edit-shop');
  }

  onChatWithAdmin() {
    this.navCtrl.navigateForward('/master/tabs/4/chat');
  }

  onEditMaster() {
    this.navCtrl.navigateForward('/master/tabs/4/edit-master');
  }

  onEditNews() {
    this.navCtrl.navigateForward('/master/tabs/4/edit-news');
  }

  onApproveShop() {
    this.navCtrl.navigateForward('/master/tabs/4/approve-shop');
  }

  onQRScanner() {}
}
