import { Component, OnInit } from '@angular/core';
import { USER_LOGIN } from 'src/interface/USER_LOGIN';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
  ACCESS_ID = 0;
  TEMP_USER: USER_LOGIN;

  ngOnInit() {
    this.TEMP_USER = JSON.parse(localStorage.getItem('user_token'));
    this.ACCESS_ID = this.TEMP_USER.ACCESS_ID;
    console.log('ACCESS_ID', this.ACCESS_ID);
  }
}
