import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: '1',
        children: [
          {
            path: '',
            loadChildren: './tab1/tab1.module#Tab1PageModule'
          }
        ]
      },
      {
        path: '2',
        children: [
          {
            path: '',
            loadChildren: './tab2/tab2.module#Tab2PageModule'
          }
        ]
      },
      {
        path: '3',
        children: [
          {
            path: '',
            loadChildren: './tab3/tab3.module#Tab3PageModule'
          }
        ]
      },
      {
        path: '4',
        children: [
          {
            path: '',
            loadChildren: './tab4/tab4.module#Tab4PageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/master/tabs/1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/master/tabs/1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
