/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Menu1Service } from './menu1.service';

describe('Service: Menu1', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Menu1Service]
    });
  });

  it('should ...', inject([Menu1Service], (service: Menu1Service) => {
    expect(service).toBeTruthy();
  }));
});
