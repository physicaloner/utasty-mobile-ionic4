import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Menu1Service {
  private prefixUrl = `${environment.apiUrl}/SHOP_INFO`;

  constructor(private http: HttpClient) {}

  getShop(pageID): Observable<any> {
    return this.http.get(`${this.prefixUrl}/getShop/${pageID}`);
  }
}
