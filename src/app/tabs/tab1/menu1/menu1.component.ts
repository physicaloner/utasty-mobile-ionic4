import { Component, OnInit } from '@angular/core';
import { SHOP_LIST } from 'src/interface/SHOP_LIST';
import { Menu1Service } from './menu1.service';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  selection = 'Phuket';
  ITEMS: SHOP_LIST[];

  constructor(private service: Menu1Service) {}

  ngOnInit() {
    this.service.getShop(1).subscribe(
      (data: SHOP_LIST[]) => {
        this.ITEMS = data;
      },
      error => {}
    );
  }

  onClick(index) {}
}
