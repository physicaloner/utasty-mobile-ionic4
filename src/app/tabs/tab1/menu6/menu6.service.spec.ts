/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Menu6Service } from './menu6.service';

describe('Service: Menu6', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Menu6Service]
    });
  });

  it('should ...', inject([Menu6Service], (service: Menu6Service) => {
    expect(service).toBeTruthy();
  }));
});
