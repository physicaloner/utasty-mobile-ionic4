import { Component, OnInit } from '@angular/core';
import { SHOP_LIST } from 'src/interface/SHOP_LIST';
import { Menu6Service } from './menu6.service';

@Component({
  selector: 'app-menu6',
  templateUrl: './menu6.component.html',
  styleUrls: ['./menu6.component.scss']
})
export class Menu6Component implements OnInit {
  ITEMS: SHOP_LIST[];

  constructor(private service: Menu6Service) {}

  ngOnInit() {
    this.service.getShop(1).subscribe(
      (data: SHOP_LIST[]) => {
        this.ITEMS = data;
      },
      error => {}
    );
  }

  onClick(index) {}
}
