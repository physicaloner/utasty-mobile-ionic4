/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Menu3Service } from './menu3.service';

describe('Service: Menu3', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Menu3Service]
    });
  });

  it('should ...', inject([Menu3Service], (service: Menu3Service) => {
    expect(service).toBeTruthy();
  }));
});
