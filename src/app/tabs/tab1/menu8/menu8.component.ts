import { Component, OnInit } from '@angular/core';
import { SHOP_LIST } from 'src/interface/SHOP_LIST';
import { Menu8Service } from './menu8.service';

@Component({
  selector: 'app-menu8',
  templateUrl: './menu8.component.html',
  styleUrls: ['./menu8.component.scss']
})
export class Menu8Component implements OnInit {
  ITEMS: SHOP_LIST[];

  constructor(private service: Menu8Service) {}

  ngOnInit() {
    this.service.getShop(1).subscribe(
      (data: SHOP_LIST[]) => {
        this.ITEMS = data;
      },
      error => {}
    );
  }

  onClick(index) {}
}
