/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Menu8Service } from './menu8.service';

describe('Service: Menu8', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Menu8Service]
    });
  });

  it('should ...', inject([Menu8Service], (service: Menu8Service) => {
    expect(service).toBeTruthy();
  }));
});
