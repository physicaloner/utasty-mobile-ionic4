/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Menu5Component } from './menu5.component';

describe('Menu5Component', () => {
  let component: Menu5Component;
  let fixture: ComponentFixture<Menu5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
