import { Component, OnInit } from '@angular/core';
import { SHOP_LIST } from 'src/interface/SHOP_LIST';
import { Menu5Service } from './menu5.service';

@Component({
  selector: 'app-menu5',
  templateUrl: './menu5.component.html',
  styleUrls: ['./menu5.component.scss']
})
export class Menu5Component implements OnInit {
  ITEMS: SHOP_LIST[];

  constructor(private service: Menu5Service) {}

  ngOnInit() {
    this.service.getShop(1).subscribe(
      (data: SHOP_LIST[]) => {
        this.ITEMS = data;
      },
      error => {}
    );
  }

  onClick(index) {}
}
