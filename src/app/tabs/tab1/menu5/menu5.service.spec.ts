/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Menu5Service } from './menu5.service';

describe('Service: Menu5', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Menu5Service]
    });
  });

  it('should ...', inject([Menu5Service], (service: Menu5Service) => {
    expect(service).toBeTruthy();
  }));
});
