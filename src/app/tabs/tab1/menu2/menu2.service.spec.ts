/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Menu2Service } from './menu2.service';

describe('Service: Menu2', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Menu2Service]
    });
  });

  it('should ...', inject([Menu2Service], (service: Menu2Service) => {
    expect(service).toBeTruthy();
  }));
});
