import { Component, OnInit } from '@angular/core';
import { SHOP_LIST } from 'src/interface/SHOP_LIST';
import { Menu2Service } from './menu2.service';

@Component({
  selector: 'app-menu2',
  templateUrl: './menu2.component.html',
  styleUrls: ['./menu2.component.scss']
})
export class Menu2Component implements OnInit {
  ITEMS: SHOP_LIST[];

  constructor(private service: Menu2Service) {}

  ngOnInit() {
    this.service.getShop(1).subscribe(
      (data: SHOP_LIST[]) => {
        this.ITEMS = data;
      },
      error => {}
    );
  }

  onClick(index) {}
}
