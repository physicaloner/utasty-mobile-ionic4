import { Component, OnInit } from '@angular/core';
import { SHOP_LIST } from 'src/interface/SHOP_LIST';
import { Menu7Service } from './menu7.service';

@Component({
  selector: 'app-menu7',
  templateUrl: './menu7.component.html',
  styleUrls: ['./menu7.component.scss']
})
export class Menu7Component implements OnInit {
  ITEMS: SHOP_LIST[];

  constructor(private service: Menu7Service) {}

  ngOnInit() {
    this.service.getShop(1).subscribe(
      (data: SHOP_LIST[]) => {
        this.ITEMS = data;
      },
      error => {}
    );
  }

  onClick(index) {}
}
