/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Menu7Service } from './menu7.service';

describe('Service: Menu7', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Menu7Service]
    });
  });

  it('should ...', inject([Menu7Service], (service: Menu7Service) => {
    expect(service).toBeTruthy();
  }));
});
