import { Component, OnInit } from '@angular/core';
import { SHOP_LIST } from 'src/interface/SHOP_LIST';
import { Menu9Service } from './menu9.service';

@Component({
  selector: 'app-menu9',
  templateUrl: './menu9.component.html',
  styleUrls: ['./menu9.component.scss']
})
export class Menu9Component implements OnInit {
  ITEMS: SHOP_LIST[];

  constructor(private service: Menu9Service) {}

  ngOnInit() {
    this.service.getShop(1).subscribe(
      (data: SHOP_LIST[]) => {
        this.ITEMS = data;
      },
      error => {}
    );
  }

  onClick(index) {}
}
