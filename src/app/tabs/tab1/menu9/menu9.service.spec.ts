/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Menu9Service } from './menu9.service';

describe('Service: Menu9', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Menu9Service]
    });
  });

  it('should ...', inject([Menu9Service], (service: Menu9Service) => {
    expect(service).toBeTruthy();
  }));
});
