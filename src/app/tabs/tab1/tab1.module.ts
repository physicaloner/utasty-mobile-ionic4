import { IonicModule } from '@ionic/angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { Tab1Routes } from './tab1.routing';
import { Menu1Component } from './menu1/menu1.component';
import { Menu2Component } from './menu2/menu2.component';
import { Menu3Component } from './menu3/menu3.component';
import { Menu4Component } from './menu4/menu4.component';
import { Menu5Component } from './menu5/menu5.component';
import { Menu6Component } from './menu6/menu6.component';
import { Menu7Component } from './menu7/menu7.component';
import { Menu8Component } from './menu8/menu8.component';
import { Menu9Component } from './menu9/menu9.component';
import { MainComponent } from './main/main.component';

@NgModule({
  imports: [
    IonicModule,
    SharedModule,
    Tab1Routes
    // RouterModule.forChild([{ path: '', component: Tab1Page }])
  ],
  declarations: [
    MainComponent,
    Menu1Component,
    Menu2Component,
    Menu3Component,
    Menu4Component,
    Menu5Component,
    Menu6Component,
    Menu7Component,
    Menu8Component,
    Menu9Component
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Tab1PageModule {}
