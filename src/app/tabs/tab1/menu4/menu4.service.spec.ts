/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Menu4Service } from './menu4.service';

describe('Service: Menu4', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Menu4Service]
    });
  });

  it('should ...', inject([Menu4Service], (service: Menu4Service) => {
    expect(service).toBeTruthy();
  }));
});
