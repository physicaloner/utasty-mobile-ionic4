import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  selection = 'Phuket';

  constructor(public navCtrl: NavController) { }

  ngOnInit() {
  }

  onClick(index) {
    this.navCtrl.navigateForward(`/master/tabs/1/menu${index}`);
    // switch(index)
    // {
    //   case 1: {
    //     break;
    //   }
    //   case 2: {
    //     break;
    //   }
    //   case 3: {
    //     break;
    //   }
    //   case 4: {
    //     break;
    //   }
    //   case 5: {
    //     break;
    //   }
    //   case 6: {
    //     break;
    //   }
    //   case 7: {
    //     break;
    //   }
    //   case 8: {
    //     break;
    //   }
    //   case 9: {
    //     break;
    //   }
    // }
  }

}
